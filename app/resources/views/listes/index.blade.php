@extends('layouts.base')

@section('titre')
    Mes listes
@endsection

@section('actions')
    <!-- Créer une liste -->
    <button class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#modalNouvelleListe">Créer une liste...</button>
    <div class="modal fade text-start" id="modalNouvelleListe" tabindex="-1">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Nouvelle liste</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <form method="post" action="{{ route('listes.store') }}">
                    @csrf
                    <div class="modal-body">
                        <div class="mb-3">
                            <label for="recipient-name" class="col-form-label">Nom de la liste :</label>
                            <input type="text" name="nomListe" class="form-control">
                        </div>
                        <div class="form-check form-switch">
                            <input class="form-check-input" name="listePublique" value="1" type="checkbox" id="flexSwitchCheckDefault">
                            <label class="form-check-label" for="flexSwitchCheckDefault">Liste publique</label>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-success">Créer</button>
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Annuler</button>
                    </div>
                </form>
            </div>
        </div>
    </div>


    <!-- Importer une liste XML -->
    <button class="btn btn-warning" data-bs-toggle="modal" data-bs-target="#modalImporterListe">Importer une liste...</button>
    <div class="modal fade text-start" id="modalImporterListe" tabindex="-1">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Importer une liste</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="card bg-light m-3">
                    <div class="card-body">
                        <p>Utilisez ce template pour créer vos fichiers.</p>
                        <code><pre>&lt;&excl;--&quest;xml version&equals;&quot;1&period;0&quot; &quest;--&gt;
&lt;data&gt;
    &lt;liste&gt;Liste XML Template&lt;&sol;liste&gt;
    &lt;items&gt;
        &lt;item qte&equals;&quot;10&quot;&gt;Item 1&lt;&sol;item&gt;
        &lt;item qte&equals;&quot;20&quot;&gt;Item 2&lt;&sol;item&gt;
        &lt;item qte&equals;&quot;30&quot;&gt;Item 3&lt;&sol;item&gt;
    &lt;&sol;items&gt;
&lt;&sol;data&gt;</pre></code>
                    </div>
                </div>
                <form method="post" enctype="multipart/form-data" action="{{ route('listes.import') }}">
                    @csrf
                    <div class="modal-body">
                        <div class="mb-3">
                            <label for="recipient-name" class="col-form-label">Fichier XML à importer :</label>
                            <input type="file"  name="fichierXML" class="form-control">
                        </div>
{{--                        <div class="form-check form-switch">--}}
{{--                            <input class="form-check-input" name="listePublique" value="1" type="checkbox" id="flexSwitchCheckDefault">--}}
{{--                            <label class="form-check-label" for="flexSwitchCheckDefault">Liste publique</label>--}}
{{--                        </div>--}}
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-success">Importer</button>
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Annuler</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('contenu')
    <table class="table table-hover align-baseline">
        <colgroup>
            <col style="width: 150px" />
        </colgroup>
        <thead>
            <tr>
                <th scope="col"></th>
                <th scope="col">Nom de la liste</th>
                <th>&nbsp;</th>
            </tr>
        </thead>
        <tbody>
        @foreach($listes as $liste)
            <tr>
                <th>{{ $liste->public ? 'Liste publique' : '' }}</th>
                <td>{!! $liste->nom !!}</td>
                <td>
                    <!-- Voir la liste -->
                    <a href="{{ route('listes.show', $liste->id) }}" class="btn btn-secondary"><span class="material-icons">visibility</span></a>

                    <!-- Supprimer la liste -->
                    @if ($liste->user->id == Auth::user()->id)
                    <button type="button" class="btn btn-danger" data-bs-toggle="modal" data-bs-target="#modal-delete-{{ $liste->id }}">
                        <span class="material-icons">delete</span>
                    </button>
                    <div class="modal fade" id="modal-delete-{{ $liste->id }}" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="modal-delete-{{ $liste->id }}Label" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="modal-delete-{{ $liste->id }}Label">Liste {{ $liste->nom }}</h5>
                                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                </div>
                                <div class="modal-body">
                                    Êtes-vous certain de vouloir supprimer la liste ?
                                </div>
                                <div class="modal-footer">
                                    <form class="d-inline" method="post" action="{{ route('listes.destroy', $liste->id) }}">
                                        @method('DELETE')
                                        @csrf
                                        <button class="btn btn-danger">Supprimer</button>
                                    </form>
                                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Annuler</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endif

                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
@endsection
