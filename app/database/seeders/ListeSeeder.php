<?php

namespace Database\Seeders;

use App\Models\Item;
use App\Models\Liste;
use App\Models\User;
use Illuminate\Database\Seeder;

class ListeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($j = 1; $j <= 3; $j++) {
            $u = User::find($j);
            for ($i = 1; $i < 10; $i++) {
                $liste = Liste::create(['user_id' => $j, 'nom' => 'Liste ' . $u->name . ' ' . $i]);

                for ($k = 1; $k <= 3; $k++) {
                    $item = Item::create(['liste_id' => $liste->id, 'nom' => 'Item ' . $k, 'quantite' => 1]);
                }
            }
        }
    }
}
